# tech infrastructure
- google cloud
- kubernetes
- pulumi
- helm


## steps
- purchased ynarwal.site with godaddy for $3.99 (year)
    - to review next year if I want to continue using that email

- enable billing on google cloud account
- account used
    - yanrwal10@gmail.com
- login with gcloud init (browser login)
- specified default cluster australia-southeast1-b
- used gcloud command to create `narwalsitecluster`
```
gcloud beta container clusters create narwalsitecluster \
    --release-channel stable
```
