# purpose
- purpose of this repo is to have project ideas written and research links and notes.

# projects
- [cleaning-business](projects/cleaning-business/index.md)
- [personal-growth](projects/personal-growth/index.md)
- [career-growth](projects/career-growth/index.md)

# useful links
- https://github.com/VGraupera/1on1-questions
- https://www.joisig.com/rules-software-startup-minimum-hassle
- https://www.linkedin.com/company/medadvisor-international-pty-ltd/jobs/